Files in the root scaffolding directory are used by Composer's own scaffolding.

Files in subdirectories are used by Robo's 'project:create' task.
