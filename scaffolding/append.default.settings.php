/**
 * Private file path:
 */
$settings['file_private_path'] = '../private';

/**
 * Move config sync outside of web-directory
 */
$settings['config_sync_directory'] = '../config/sync';


/**
 * Disable permission hardening to avoid deployment problems
 */
$settings['skip_permissions_hardening'] = TRUE;

/**
 * Always trust our own dev and live server
 */
$settings['trusted_host_patterns'] = [
  '^.+\.[a-z]+\.webtourismus\.at$',
  '^.+\.dedi(103|1661|3319|4451)\.your-server\.de$',
];

/**
 * Webtourismus.at has a 2-staged environment workflow: dev => prod
 *
 * Dev enviroment can be detected by the special directory structure
 * /var/www/vhost/PLESK_HOST.webtourismus.at/PROJECT_NAME.PLESK_HOST.webtourimus.at
 * (only CLI tools) or by the special domain name structure
 * https://PROJECT_NAME.PLESK_HOST.webtourismus.at
 *
 * Anything else will be consired a live environment
 */
$settings['wt.environment'] = 'prod';

if (preg_match('/^\/var\/www\/vhosts\/[a-z]+\.webtourismus\.at\/([a-zA-Z0-9-_]+)\.[a-z]+\.webtourismus\.at(\/web)?$/', $_SERVER['PWD'], $subdomain) === 1) {
  $settings['wt.environment'] = 'dev';
  $settings['wt.subdomain'] = $subdomain[1];
}
elseif (preg_match('/^(www\.)?([a-zA-Z0-9-_]+)\.[a-z]+\.webtourismus\.at$/', $_SERVER['HTTP_HOST'], $subdomain) === 1) {
  $settings['wt.environment'] = 'dev';
  $settings['wt.subdomain'] = $subdomain[2];
}

/**
 * Performance settings
 */
$config['system.performance']['css']['preprocess'] = ($settings['wt.environment'] === 'prod');
$config['system.performance']['js']['preprocess'] = ($settings['wt.environment'] === 'prod');

/**
 * Performance settings
 */
if ($settings['wt.environment'] === 'dev') {
  /**
   * dev settings shared cross-project, includes sensitve settings
   * load from outside, so it can't accidently leak to VCS or live servers
   */
  if (file_exists($app_root . '/../../settings.dev.php')) {
    include $app_root . '/../../settings.dev.php';
  }

  /**
   * project-specific dev settings
   */
  if (file_exists($app_root . '/' . $site_path . '/settings.dev.php')) {
    include $app_root . '/' . $site_path . '/settings.dev.php';
  }

  /**
   * dev twig debugging
   */
  if (file_exists($app_root . '/' . $site_path . '/services.dev.yml')) {
    $settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.dev.yml';
  }
}
else {
  /**
   * project-specific live settings
   */
  if (file_exists($app_root . '/' . $site_path . '/settings.prod.php')) {
    include $app_root . '/' . $site_path . '/settings.prod.php';
  }
}
