/**
 * @file
 * Webtourismus Frontend behaviors.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';
  drupalSettings.wt = drupalSettings.wt || {};

  /**
   * Behavior description.
   */
  Drupal.behaviors.wt_test = {
    attach: function (context, settings) {

      console.log('Child theme loaded!');

    }
  };

} (jQuery, Drupal, drupalSettings));
