<?php 

$settings['trusted_host_patterns'][] = '^.+\.FIXXXME\.COM$';

$databases['default']['default'] = array (
  'database' => 'FIXXXME',
  'username' => 'FIXXXME',
  'password' => 'FIXXXME',
  'prefix' => '',
  'host' => 'localhost',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
