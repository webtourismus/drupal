#!/usr/bin/env sh

sass ./web/themes/custom/wt_theme/scss/_index.scss ./web/themes/custom/wt_theme/css/wt_theme.css --style expanded
sass ./web/themes/custom/wt_theme/scss/layout_builder_only.scss ./web/themes/custom/wt_theme/css/layout_builder_only.css --style expanded
